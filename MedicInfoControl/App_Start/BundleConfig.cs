﻿using System.Web;
using System.Web.Optimization;


public class BundleConfig {
  // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
  public static void RegisterBundles(BundleCollection bundles) {
    bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Content/admin-lte/js/jquery.min.js"));

    bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

    // Use the development version of Modernizr to develop with and learn from. Then, when you're
    // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
    bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

    bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
      "~/Scripts/jquery-3.4.1.js",
      "~/Scripts/jquery-ui-1.12.1.js",
      "~/Scripts/jquery.validate.js",
      "~/Scripts/jquery.validate.unobtrusive.js",
      "~/Scripts/respond.js",
      "~/Scripts/moment.js",
      "~/Scripts/canvasjs.min.js",
      "~/Scripts/jquery.canvasjs.min",
      "~/Content/admin-lte/js/bootstrap.min.js",
      "~/Content/admin-lte/js/adminlte.min.js",
      "~/Content/admin-lte/js/icheck.min.js",
      "~/Scripts/toastr.min.js",
      "~/Scripts/daypilot-all.min.js"
      ));
    bundles.Add(new ScriptBundle("~/bundles/scripts/datatable").Include(
                 "~/Scripts/DataTables/jquery.dataTables.js",
                 "~/Scripts/DataTables/dataTables.responsive.js",
                 "~/Scripts/DataTables/dataTables.buttons.js",
                 "~/Scripts/DataTables/dataTables.js",
                 "~/Scripts/DataTables/dataTables.select.js",
                 "~/Scripts/DataTables/dataTables.fixedColumns.js",
                 "~/Scripts/DataTables/dataTables.fixedHeader.js",
                 "~/Scripts/DataTables/buttons.html5.js",
                 "~/Scripts/DataTables/buttons.print.js",
                 "~/Scripts/DataTables/dataTables.scroller.js",
                 "~/Scripts/DataTables/dataTables.rowGroup.js",
                 "~/Scripts/DataTables/dataTables.bootstrap4.js",
                 "~/Scripts/DataTables/dataTables.keyTable.js",
                 "~/Scripts/DataTables/jszip.min.js",
                 "~/Scripts/DataTables/vfs_fonts.js",
                 "~/Scripts/DataTables/colReorder.js",
                 "~/Scripts/DataTables/dataTables.colResize.js",
                 "~/Scripts/DataTables/dataTables.colVis.js"
                 ));

    bundles.Add(new StyleBundle("~/Content/css").Include(
              "~/Content/site.css",
              "~/Content/jquery-ui.css",
              "~/Content/jquery-ui.structure.css",
              "~/Content/jquery-ui.theme.css",
              "~/Content/admin-lte/css/bootstrap.min.css",
              "~/Content/admin-lte/css/font-awesome.min.css",
              "~/Content/admin-lte/css/ionicons.min.css",
              "~/Content/admin-lte/css/AdminLTE.min.css",
              "~/Content/admin-lte/css/skin-blue.min.css",
              "~/Content/admin-lte/css/blue.css",
              "~/Content/toastr.min.css"));
    bundles.Add(new StyleBundle("~/Content/DataTables/css/bundle").Include(
      "~/Content/DataTables/css/jquery.dataTables.min.css",
      "~/Content/DataTables/css/dataTables.jqueryui.min.css",
      "~/Content/DataTables/css/dataTables.bootstrap4.css",
      "~/Content/DataTables/css/responsive.bootstrap4.css",
      "~/Content/DataTables/css/fixedColumns.bootstrap4.css",
      "~/Content/DataTables/css/fixedHeader.bootstrap4.css",
      "~/Content/DataTables/css/rowGroup.bootstrap4.css",
      "~/Content/DataTables/css/keyTable.bootstrap4.css",
      "~/Content/DataTables/css/buttons.dataTables.css",
      "~/Content/DataTables/css/responsive.bootstrap4.css",
      "~/Content/DataTables/css/select.bootstrap4.css",
      "~/Content/DataTables/css/colReorder.bootstrap4.css"
      ));
  }
}

