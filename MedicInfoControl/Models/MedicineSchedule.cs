﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedicInfoControl.Models {
  [Table("medicineschedule")]
  public class MedicineSchedule {
    public int id { get; set; }
    public int medicineId { get; set; }
    public int vodUserId { get; set; }
    [DisplayName("Эхлэх")]
    public DateTime startDt { get; set; }
    [DisplayName("Дуусах")]
    public DateTime endDt { get; set; }

    [NotMapped]
    [DisplayName("Эмийн нэр")]
    public string medicineName { get; set; }
    [NotMapped]
    public String customerName { get; set; }
    [NotMapped]
    public String startDts { get; set; }
    [NotMapped]
    public String endDts { get; set; }
    [NotMapped]
    public String howToUse { get; set; }
    [NotMapped]
    public String drunkYN { get; set; }
    public IEnumerable<SelectListItem> cusListBox { get; set; }
    public IEnumerable<SelectListItem> medListBox { get; set; }
  }
}