﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedicInfoControl.Models {
  [Table("district")]
  public class District {
    [Key]
    public int id { get; set; }
    [DisplayName("Сум/Дүүргийн нэр")]
    [Required(ErrorMessage = "Сум/Дүүргийн нэрээ оруулна уу!")]
    [StringLength(50, ErrorMessage = "Талбарын урт хамгийн уртдаа 50 байна.")]
    public string name { get; set; }
    [DisplayName("Сум/Дүүргийн код")]
    [Required(ErrorMessage = "Сум/Дүүргийн кодоо оруулна уу!")]
    [StringLength(50, ErrorMessage = "Талбарын урт хамгийн уртдаа 50 байна.")]
    [Remote("districtExistCode", "District", AdditionalFields = "id", HttpMethod = "POST", ErrorMessage = "Код бүртгэгдсэн тул солино уу!")]
    public string code { get; set; }
    [DisplayName("Аймаг/Нийслэл")]
    [Required(ErrorMessage = "Аймаг/Нийслэл сонгоно уу!")]
    public int? provinceId { get; set; }
    [NotMapped]
    public string provinceName { get; set; }
    public IEnumerable<SelectListItem> provListBox { get; set; }
  }
}