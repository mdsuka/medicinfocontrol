﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MedicInfoControl.Models {
  [Table("treatmentresult")]
  public class TreatmentResult {
    public int id { get; set; }
    public DateTime drinkDt { get; set; }
    public int medschedId { get; set; }
  }
}