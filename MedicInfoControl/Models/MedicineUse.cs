﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MedicInfoControl.Models {
  [Table("medicineuse")]
  public class MedicineUse {
    [Key]
    public int medicineId { get; set; }
    [DisplayName("Эмийн нэр(MNG)")]
    public string medicineNameMng { get; set; }
    [DisplayName("Эмийн нэр(ENG)")]
    public string medicineNameEng { get; set; }
    [DisplayName("Тун хэмжээ")]
    public string medicineUnit { get; set; }
    [DisplayName("Хэдийд хэрэгэх")]
    public string treatmentName { get; set; }
    [DisplayName("Уух давтамж")]
    public int frequencyMed { get; set; }
  }
}