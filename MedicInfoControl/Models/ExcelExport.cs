﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;

namespace MedicInfoControl.Models
{
	public class ExcelExport
	{
		public static string ExcelContentType
		{
			get { return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; }
		}
		public static DataTable ListToDataTable<T>(List<T> data, string repType = "")
		{
			PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
			DataTable dataTable = new DataTable();
			for (int i = 0; i < properties.Count; i++)
			{
				PropertyDescriptor property = properties[i];
				dataTable.Columns.Add(property.Name, Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType);
			}
			object[] values = new object[properties.Count];
			if (dataTable.Columns["date"] != null)
			{
				dataTable.Columns["date"].ColumnName = "Огноо";
			}
			foreach (T item in data)
			{
				for (int i = 0; i < values.Length; i++)
				{
					values[i] = properties[i].GetValue(item);
					DateTime temp;
					if (values[0] != null) {
						if (DateTime.TryParse(values[0].ToString(), out temp))
						{
							values[0] = temp.ToString("yyyy-MM-dd");
						}
					}
				}
				dataTable.Rows.Add(values);
			}

            if (repType.Equals("unalloc"))
            {
                //Зээлийн хуваарилагдаагүй төлбөр            
                if (dataTable.Columns["loanNumber"] != null)
                    dataTable.Columns["loanNumber"].ColumnName = "Зээлийн дугаар";
                if (dataTable.Columns["customerName"] != null)
                    dataTable.Columns["customerName"].ColumnName = "Харилцагчийн нэр";
                if (dataTable.Columns["date"] != null)
                    dataTable.Columns["date"].ColumnName = "Огноо";
                if (dataTable.Columns["allocatedAmount"] != null)
                    dataTable.Columns["allocatedAmount"].ColumnName = "Хуваарилагдсан дүн";
                if (dataTable.Columns["unallocatedAmount"] != null)
                    dataTable.Columns["unallocatedAmount"].ColumnName = "Хуваарилагдаагүй дүн";
                if (dataTable.Columns["mainAmount"] != null)
                    dataTable.Columns["mainAmount"].ColumnName = "Үндсэн төлбөр";
                if (dataTable.Columns["interestAmount"] != null)
                    dataTable.Columns["interestAmount"].ColumnName = "Хүүгийн төлбөр";
                if (dataTable.Columns["leverageAmount"] != null)
                    dataTable.Columns["leverageAmount"].ColumnName = "Хөшүүргийн төлбөр";
                if (dataTable.Columns["otherAmount"] != null)
                    dataTable.Columns["otherAmount"].ColumnName = "Бусад төлбөр";
                if (dataTable.Columns["loanBalance"] != null)
                    dataTable.Columns["loanBalance"].ColumnName = "Зээлийн үлдэгдэл";
                if (dataTable.Columns["transactionAmount"] != null)
                    dataTable.Columns["transactionAmount"].ColumnName = "Гүйлгээний дүн";                
                return dataTable;
            }
            if (repType.Equals("acolous"))
            {
                //Acolous Excel
                if (dataTable.Columns["time"] != null)
                    dataTable.Columns["time"].ColumnName = "Цаг";
                if (dataTable.Columns["dtAmount"] != null)
                    dataTable.Columns["dtAmount"].ColumnName = "Орлого";
                if (dataTable.Columns["ktAmount"] != null)
                    dataTable.Columns["ktAmount"].ColumnName = "Зарлага";
                if (dataTable.Columns["totalAmount"] != null)
                    dataTable.Columns["totalAmount"].ColumnName = "Эцсийн үлдэгдэл";
                if (dataTable.Columns["transactionReference"] != null)
                    dataTable.Columns["transactionReference"].ColumnName = "Гүйлгээний утга";
                if (dataTable.Columns["ecocode"] != null)
                    dataTable.Columns["ecocode"].ColumnName = "ЭЗ ангилал";
                if (dataTable.Columns["customer"] != null)
                    dataTable.Columns["customer"].ColumnName = "Байгууллага";
                if (dataTable.Columns["accountcode"] != null)
                    dataTable.Columns["accountcode"].ColumnName = "Данс";
                if (dataTable.Columns["clientcode"] != null)
                    dataTable.Columns["clientcode"].ColumnName = "Харьцсан данс";
                if (dataTable.Columns["guidebook"] != null)
                    dataTable.Columns["guidebook"].ColumnName = "Хөтөлбөр";
                if (dataTable.Columns["events"] != null)
                    dataTable.Columns["events"].ColumnName = "Арга хэмжээ";
                if (dataTable.Columns["number"] != null)
                    dataTable.Columns["number"].ColumnName = "Баримтын дугаар";
                return dataTable;
            }

            if (dataTable.Columns["bankName"] != null)
				dataTable.Columns["bankName"].ColumnName = "Банкны нэр";
			if (dataTable.Columns["bankAccount"] != null)
				dataTable.Columns["bankAccount"].ColumnName = "Банкны данс";
			if (dataTable.Columns["transactionReference"] != null)
				dataTable.Columns["transactionReference"].ColumnName = "Гүйлгээний утга";
			if (dataTable.Columns["number"] != null)
				dataTable.Columns["number"].ColumnName = "Баримтын дугаар";
			if (dataTable.Columns["accPlan"] != null)
				dataTable.Columns["accPlan"].ColumnName = "Данс";
			if (dataTable.Columns["guidBook"] != null)
				dataTable.Columns["guidBook"].ColumnName = "Хөтөлбөр";
			if (dataTable.Columns["events"] != null)
				dataTable.Columns["events"].ColumnName = "Арга хэмжээ";
			if (dataTable.Columns["sourcefunding"] != null)
				dataTable.Columns["sourcefunding"].ColumnName = "Эх үүсвэр";
			if (dataTable.Columns["economic"] != null)
				dataTable.Columns["economic"].ColumnName = "Эдийн засгийн ангилал";
			if (dataTable.Columns["dtAmount"] != null)
				dataTable.Columns["dtAmount"].ColumnName = "Орлого";
			if (dataTable.Columns["ktAmount"] != null)
				dataTable.Columns["ktAmount"].ColumnName = "Зарлага";
			if (dataTable.Columns["dtAmountCurrency"] != null)
				dataTable.Columns["dtAmountCurrency"].ColumnName = "Орлого(вал)";
			if (dataTable.Columns["ktAmountCurrency"] != null)
				dataTable.Columns["ktAmountCurrency"].ColumnName = "Зарлага(вал)";
			if (dataTable.Columns["customerCode"] != null)
				dataTable.Columns["customerCode"].ColumnName = "Харилцагчийн код";
			if (dataTable.Columns["relAcc"] != null)
				dataTable.Columns["relAcc"].ColumnName = "Харьцсан данс";
			if (dataTable.Columns["stateName"] != null)
				dataTable.Columns["stateName"].ColumnName = "Төлөв";
			if (dataTable.Columns["listTreasureDT"] != null)
				dataTable.Columns["listTreasureDT"].ColumnName = "Төрийн сангийн ДТ данс";
			if (dataTable.Columns["listTreasureKT"] != null)
				dataTable.Columns["listTreasureKT"].ColumnName = "Төрийн сангийн KТ данс";
			if (dataTable.Columns["totalAmount"] != null)
				dataTable.Columns["totalAmount"].ColumnName = "Дүн";
			if (dataTable.Columns["listDT"] != null)
				dataTable.Columns["listDT"].ColumnName = "Дебит банк данс";
			if (dataTable.Columns["listKT"] != null)
				dataTable.Columns["listKT"].ColumnName = "Кредит банк данс";
			//Ерөнхий журнал
			if (dataTable.Columns["customer"] != null)
				dataTable.Columns["customer"].ColumnName = "Харилцагч ";
			if (dataTable.Columns["AccountPlan"] != null)
				dataTable.Columns["AccountPlan"].ColumnName = "Данс ";
			if (dataTable.Columns["cusCode"] != null)
				dataTable.Columns["cusCode"].ColumnName = "Харилцагчийн код ";
			if (dataTable.Columns["dtAmountg"] != null)
				dataTable.Columns["dtAmountg"].ColumnName = "Дебит дүн";
			if (dataTable.Columns["ktAmountg"] != null)
				dataTable.Columns["ktAmountg"].ColumnName = "Кредит дүн";
			if (dataTable.Columns["jurnalType"] != null)
				dataTable.Columns["jurnalType"].ColumnName = "Журналын төрөл";
			if (dataTable.Columns["state"] != null)
				dataTable.Columns["state"].ColumnName = "Төлөв ";
			if (dataTable.Columns["date"] != null)
				dataTable.Columns["date"].ColumnName = "Огноо ";
			//Тооцооны журнал
			if (dataTable.Columns["firstdt"] != null)
				dataTable.Columns["firstdt"].ColumnName = "Эх.үл ДТ дүн";
			if (dataTable.Columns["firstkt"] != null)
				dataTable.Columns["firstkt"].ColumnName = "Эх.үл КТ дүн";
			if (dataTable.Columns["tradt"] != null)
				dataTable.Columns["tradt"].ColumnName = "Гүйлгээний ДТ дүн";
			if (dataTable.Columns["trakt"] != null)
				dataTable.Columns["trakt"].ColumnName = "Гүйлгээний КТ дүн";
			if (dataTable.Columns["enddt"] != null)
				dataTable.Columns["enddt"].ColumnName = "Эц.үл ДТ дүн";
			if (dataTable.Columns["endkt"] != null)
				dataTable.Columns["endkt"].ColumnName = "Эц.үл КТ дүн";
			if (dataTable.Columns["govName"] != null)
				dataTable.Columns["govName"].ColumnName = "Захирагч";

			//Төсөл
			if (dataTable.Columns["code"] != null)
				dataTable.Columns["code"].ColumnName = "Код";
			if (dataTable.Columns["name"] != null)
				dataTable.Columns["name"].ColumnName = "Төслийн нэр";
			if (dataTable.Columns["sponsorMap"] != null)
				dataTable.Columns["sponsorMap"].ColumnName = "Санхүүжүүлэгч";
			if (dataTable.Columns["cusMap"] != null)
				dataTable.Columns["cusMap"].ColumnName = "/Харилцагч/";
			if (dataTable.Columns["loanPurposeMap"] != null)
				dataTable.Columns["loanPurposeMap"].ColumnName = "Зээлийн зориулалт";
			if (dataTable.Columns["loanTypeMap"] != null)
				dataTable.Columns["loanTypeMap"].ColumnName = "Зээлийн төрөл";
			//Санхүүжилт
			if (dataTable.Columns["cuscode"] != null)
				dataTable.Columns["cuscode"].ColumnName = "Харилцагчийн код";
			if (dataTable.Columns["monthAmount"] != null)
				dataTable.Columns["monthAmount"].ColumnName = "Сарын төсөв";
			if (dataTable.Columns["budgovernor"] != null)
				dataTable.Columns["budgovernor"].ColumnName = "ТЕЗахирагч";
			if (dataTable.Columns["province"] != null)
				dataTable.Columns["province"].ColumnName = "Аймаг";
			if (dataTable.Columns["guideName"] != null)
				dataTable.Columns["guideName"].ColumnName = "Хөтөлбөр/нэр/";
			if (dataTable.Columns["guideCode"] != null)
				dataTable.Columns["guideCode"].ColumnName = "Хөтөлбөр/код/";
			if (dataTable.Columns["eventName"] != null)
				dataTable.Columns["eventName"].ColumnName = "Арга хэмжээ/нэр/";
			if (dataTable.Columns["eventCode"] != null)
				dataTable.Columns["eventCode"].ColumnName = "Арга хэмжээ/код/";
			if (dataTable.Columns["approvedAmount"] != null)
				dataTable.Columns["approvedAmount"].ColumnName = "Олгосон";
			if (dataTable.Columns["yearAmount"] != null)
				dataTable.Columns["yearAmount"].ColumnName = "Жилийн төсөв";
			if (dataTable.Columns["monthAmountUp"] != null)
				dataTable.Columns["monthAmountUp"].ColumnName = "Сарын төсөв/өссөн/";
			if (dataTable.Columns["performanceAmountUp"] != null)
				dataTable.Columns["performanceAmountUp"].ColumnName = "Гүйцэтгэл/өссөн/";
			if (dataTable.Columns["prepaymentAmount"] != null)
				dataTable.Columns["prepaymentAmount"].ColumnName = "Урьдчилгаа";
			if (dataTable.Columns["performanceAmount"] != null)
				dataTable.Columns["performanceAmount"].ColumnName = "Гүйцэтгэл";
			if (dataTable.Columns["nowAmount"] != null)
				dataTable.Columns["nowAmount"].ColumnName = "Одоо олгох";
			if (dataTable.Columns["afterAmount"] != null)
				dataTable.Columns["afterAmount"].ColumnName = "Олгосны дараах";
			//Төлбөвлөгөө
			if (dataTable.Columns["min_name"] != null)
				dataTable.Columns["min_name"].ColumnName = "Төсвийн захирагч";
			if (dataTable.Columns["org_name"] != null)
				dataTable.Columns["org_name"].ColumnName = " Харилцагч";
			if (dataTable.Columns["Guidebook"] != null)
				dataTable.Columns["Guidebook"].ColumnName = "Хөтөлбөр ";
			if (dataTable.Columns["Allo1"] != null)
				dataTable.Columns["Allo1"].ColumnName = "1-р сар";
			if (dataTable.Columns["Allo2"] != null)
				dataTable.Columns["Allo2"].ColumnName = "2-р сар";
			if (dataTable.Columns["Allo3"] != null)
				dataTable.Columns["Allo3"].ColumnName = "3-р сар";
			if (dataTable.Columns["Allo4"] != null)
				dataTable.Columns["Allo4"].ColumnName = "4-р сар";
			if (dataTable.Columns["Allo5"] != null)
				dataTable.Columns["Allo5"].ColumnName = "5-р сар";
			if (dataTable.Columns["Allo6"] != null)
				dataTable.Columns["Allo6"].ColumnName = "6-р сар";
			if (dataTable.Columns["Allo7"] != null)
				dataTable.Columns["Allo7"].ColumnName = "7-р сар";
			if (dataTable.Columns["Allo8"] != null)
				dataTable.Columns["Allo8"].ColumnName = "8-р сар";
			if (dataTable.Columns["Allo9"] != null)
				dataTable.Columns["Allo9"].ColumnName = "9-р сар";
			if (dataTable.Columns["Allo10"] != null)
				dataTable.Columns["Allo10"].ColumnName = "10-р сар";
			if (dataTable.Columns["Allo11"] != null)
				dataTable.Columns["Allo11"].ColumnName = "11-р сар";
			if (dataTable.Columns["Allo12"] != null)
				dataTable.Columns["Allo12"].ColumnName = "12-р сар";
			if (dataTable.Columns["monthTotal"] != null)
				dataTable.Columns["monthTotal"].ColumnName = "Нийлбэр";
			if (dataTable.Columns["importDate"] != null)
				dataTable.Columns["importDate"].ColumnName = "Огноо";
			if (dataTable.Columns["accCode"] != null)
				dataTable.Columns["accCode"].ColumnName = " Данс";
			if (dataTable.Columns["accName"] != null)
				dataTable.Columns["accName"].ColumnName = "Дансны нэр";
			if (dataTable.Columns["projectN"] != null)
				dataTable.Columns["projectN"].ColumnName = "Төсөл";
			if (dataTable.Columns["currencyType"] != null)
				dataTable.Columns["currencyType"].ColumnName = "Валютын төрөл";
			if (dataTable.Columns["rate"] != null)
				dataTable.Columns["rate"].ColumnName = "Ханш";
			if (dataTable.Columns["currencyEndBalance"] != null)
				dataTable.Columns["currencyEndBalance"].ColumnName = "Үлдэгдэл/Вал";
			if (dataTable.Columns["endBalance"] != null)
				dataTable.Columns["endBalance"].ColumnName = "Үлдэгдэл/Төг";
			if (dataTable.Columns["diffExchangeRate"] != null)
				dataTable.Columns["diffExchangeRate"].ColumnName = "Тэгшитгэх дүн";
			if (dataTable.Columns["equivalentBalance"] != null)
				dataTable.Columns["equivalentBalance"].ColumnName = "Шинэ үлдэгдэл/Төг";
			if (dataTable.Columns["stateMap"] != null)
				dataTable.Columns["stateMap"].ColumnName = " Төлөв";
			if (dataTable.Columns["jurnalCode"] != null)
				dataTable.Columns["jurnalCode"].ColumnName = "Харьцсан данс";
			if (dataTable.Columns["accountCode"] != null)
				dataTable.Columns["accountCode"].ColumnName = "Дансны код";
			if (dataTable.Columns["accountName"] != null)
				dataTable.Columns["accountName"].ColumnName = "Дансны нэр";
			if (dataTable.Columns["allDt"] != null)
				dataTable.Columns["allDt"].ColumnName = "Гүйлгээний ДТ дүн ";
			if (dataTable.Columns["allKt"] != null)
				dataTable.Columns["allKt"].ColumnName = "Гүйлгээний КТ дүн ";
			if (dataTable.Columns["setDt"] != null)
				dataTable.Columns["setDt"].ColumnName = "Тох/Бичилт ДТ дүн";
			if (dataTable.Columns["setKt"] != null)
				dataTable.Columns["setKt"].ColumnName = "Тох/Бичилт КТ дүн";
			if (dataTable.Columns["ozDt"] != null)
				dataTable.Columns["ozDt"].ColumnName = "ОЗНД ДТ дүн";
			if (dataTable.Columns["ozKt"] != null)
				dataTable.Columns["ozKt"].ColumnName = "ОЗНД КТ дүн";
			if (dataTable.Columns["invDt"] != null)
				dataTable.Columns["invDt"].ColumnName = "Үл/Баланс ДТ дүн";
			if (dataTable.Columns["invKt"] != null)
				dataTable.Columns["invKt"].ColumnName = "Үл/Баланс КТ дүн";
			//Дансны төлөвлөгөө
			if (dataTable.Columns["accountPlanName"] != null)
				dataTable.Columns["accountPlanName"].ColumnName = "ДТ-ний нэр";
			if (dataTable.Columns["parantName"] != null)
				dataTable.Columns["parantName"].ColumnName = "Үндсэн код";
			if (dataTable.Columns["accountPlanTypeName"] != null)
				dataTable.Columns["accountPlanTypeName"].ColumnName = "ДТ-ний төрөл";
			if (dataTable.Columns["currencyTypeName"] != null)
				dataTable.Columns["currencyTypeName"].ColumnName = "Валютын төрөл ";
			if (dataTable.Columns["economicLevelName"] != null)
				dataTable.Columns["economicLevelName"].ColumnName = "ЭЗ-н ангилал";

			// Харилцагчийн жагсаалт
			if (dataTable.Columns["code"] != null)
				dataTable.Columns["code"].ColumnName = "Код";
			if (dataTable.Columns["cusname"] != null)
				dataTable.Columns["cusname"].ColumnName = "Харилцагч";
			if (dataTable.Columns["custp"] != null)
				dataTable.Columns["custp"].ColumnName = "Харилцагчийн төрөл";
			if (dataTable.Columns["regNumber"] != null)
				dataTable.Columns["regNumber"].ColumnName = "Регистр";
			if (dataTable.Columns["cntr"] != null)
				dataTable.Columns["cntr"].ColumnName = "Улс";
			if (dataTable.Columns["prv"] != null)
				dataTable.Columns["prv"].ColumnName = "Аймаг/Нийслэл";
			if (dataTable.Columns["dstr"] != null)
				dataTable.Columns["dstr"].ColumnName = "Сум дүүрэг";
			if (dataTable.Columns["prefx"] != null)
				dataTable.Columns["prefx"].ColumnName = "Товч нэр";
			if (dataTable.Columns["enname"] != null)
				dataTable.Columns["enname"].ColumnName = "Латин нэр";
			if (dataTable.Columns["state"] != null)
				dataTable.Columns["state"].ColumnName = "Байгууллагын төрөл";
			if (dataTable.Columns["certid"] != null)
				dataTable.Columns["certid"].ColumnName = "УБГД";
			if (dataTable.Columns["txp"] != null)
				dataTable.Columns["txp"].ColumnName = "ТТБД";
			if (dataTable.Columns["bgname"] != null)
				dataTable.Columns["bgname"].ColumnName = "Төсвийн захирагч ";
			if (dataTable.Columns["incat"] != null)
				dataTable.Columns["incat"].ColumnName = "Дотоод ангилал";
			if (dataTable.Columns["address"] != null)
				dataTable.Columns["address"].ColumnName = "Хаяг";
			if (dataTable.Columns["act"] != null)
				dataTable.Columns["act"].ColumnName = "Үйл ажиллагааны чиглэл";
			if (dataTable.Columns["phn"] != null)
				dataTable.Columns["phn"].ColumnName = "Утасны дугаар";
			if (dataTable.Columns["eml"] != null)
				dataTable.Columns["eml"].ColumnName = "И-мэйл";
			if (dataTable.Columns["ctdt"] != null)
				dataTable.Columns["ctdt"].ColumnName = "Гэрээний хугацаа";
			if (dataTable.Columns["bankInfo"] != null)
				dataTable.Columns["bankInfo"].ColumnName = " Дансны нэр";

			// Орлогын дансны тохиргоо
			if (dataTable.Columns["treasuriesAccountMap"] != null)
				dataTable.Columns["treasuriesAccountMap"].ColumnName = "Банк данс";
			if (dataTable.Columns["treasuriesAccName"] != null)
				dataTable.Columns["treasuriesAccName"].ColumnName = "Банк дансны нэр";
			if (dataTable.Columns["debitAccMap"] != null)
				dataTable.Columns["debitAccMap"].ColumnName = "Дт данс";
			if (dataTable.Columns["debitAccName"] != null)
				dataTable.Columns["debitAccName"].ColumnName = "Дт дансны нэр";
			if (dataTable.Columns["creditAccMap"] != null)
				dataTable.Columns["creditAccMap"].ColumnName = "Кт данс";
			if (dataTable.Columns["creditAccName"] != null)
				dataTable.Columns["creditAccName"].ColumnName = "Кт дансны нэр";
			if (dataTable.Columns["economicMap"] != null)
				dataTable.Columns["economicMap"].ColumnName = "ЭЗА-н код";
			if (dataTable.Columns["economicName"] != null)
				dataTable.Columns["economicName"].ColumnName = "ЭЗА-н нэр";
			if (dataTable.Columns["customerName"] != null)
				dataTable.Columns["customerName"].ColumnName = "Харилцагч нэр";
			if (dataTable.Columns["accountTypeName"] != null)
				dataTable.Columns["accountTypeName"].ColumnName = "Гүйлгээний төрөл";
			// Банк данс
			if (dataTable.Columns["budGovernorName"] != null)
				dataTable.Columns["budGovernorName"].ColumnName = " Төсвийн захирагч";
			if (dataTable.Columns["accountNumber"] != null)
				dataTable.Columns["accountNumber"].ColumnName = "Дансны дугаар";
			if (dataTable.Columns["orgaNameMap"] != null)
				dataTable.Columns["orgaNameMap"].ColumnName = "Байгууллагын нэр";
			if (dataTable.Columns["customerMap"] != null)
				dataTable.Columns["customerMap"].ColumnName = "Харилцагчийн нэр";
			// Гэрээ
			if (dataTable.Columns["mainDocId"] != null)
				dataTable.Columns["mainDocId"].ColumnName = "ҮББ";
			if (dataTable.Columns["contractNumber"] != null)
				dataTable.Columns["contractNumber"].ColumnName = "Дугаар";
			if (dataTable.Columns["cusID"] != null)
				dataTable.Columns["cusID"].ColumnName = "харилцагч";
			if (dataTable.Columns["projectId"] != null)
				dataTable.Columns["projectId"].ColumnName = "Төсөл ";
			if (dataTable.Columns["sourceFunId"] != null)
				dataTable.Columns["sourceFunId"].ColumnName = "Зээлийн эх үүсвэр";
			if (dataTable.Columns["contractMonth"] != null)
				dataTable.Columns["contractMonth"].ColumnName = "Хугацаа сар";
			if (dataTable.Columns["amount"] != null)
				dataTable.Columns["amount"].ColumnName = "Нийт дүн";
			if (dataTable.Columns["repayFormId"] != null)
				dataTable.Columns["repayFormId"].ColumnName = "ЭТХ";
			//Гүйцэтгэл
			if (dataTable.Columns["date"] != null)
				dataTable.Columns["date"].ColumnName = "Огноо";
			if (dataTable.Columns["provinceName"] != null)
				dataTable.Columns["provinceName"].ColumnName = "Аймаг/хот";
			if (dataTable.Columns["cusCode"] != null)
				dataTable.Columns["cusCode"].ColumnName = "Харилцагчийн код";
			if (dataTable.Columns["cusName"] != null)
				dataTable.Columns["cusName"].ColumnName = "Харилцагч";
			if (dataTable.Columns["dt"] != null)
				dataTable.Columns["dt"].ColumnName = "ДТ данс";
			if (dataTable.Columns["kt"] != null)
				dataTable.Columns["kt"].ColumnName = "КТ данс";
			if (dataTable.Columns["month"] != null)
				dataTable.Columns["month"].ColumnName = "Сар";
			if (dataTable.Columns["inwardlabel"] != null)
				dataTable.Columns["inwardlabel"].ColumnName = "Дотоод ангилал";
			if (dataTable.Columns["description"] != null)
				dataTable.Columns["description"].ColumnName = "Тайлбар";
			//Санхүүжилтийн мэдэгдэл
			if (dataTable.Columns["loanNumber"] != null)
				dataTable.Columns["loanNumber"].ColumnName = "Зээлийн дугаар";
			if (dataTable.Columns["cusbankName"] != null)
				dataTable.Columns["cusbankName"].ColumnName = "Шилжүүлэх дансны нэр, дугаар";
			if (dataTable.Columns["wholeName"] != null)
				dataTable.Columns["wholeName"].ColumnName = " Валютын төрөл";
			if (dataTable.Columns["sumAmount"] != null)
				dataTable.Columns["sumAmount"].ColumnName = "Төлөх дүн";
			if (dataTable.Columns["paymentPurpose"] != null)
				dataTable.Columns["paymentPurpose"].ColumnName = "Төлбөрийн зориулалт";
			if (dataTable.Columns["tranDate"] != null)
				dataTable.Columns["tranDate"].ColumnName = "Олгосон огноо";
			//Зээл
			if (dataTable.Columns["interest"] != null)
				dataTable.Columns["interest"].ColumnName = "Зээлийн хүү";
			if (dataTable.Columns["loanTime"] != null)
				dataTable.Columns["loanTime"].ColumnName = "Зээлийн хугацаа";
			if (dataTable.Columns["loanDate"] != null)
				dataTable.Columns["loanDate"].ColumnName = "Зээл олгосон огноо";
			if (dataTable.Columns["ctractNumber"] != null)
				dataTable.Columns["ctractNumber"].ColumnName = "Гэрээний дугаар";
			//Эхний үлдэгдэл
			if (dataTable.Columns["mainAmount"] != null)
				dataTable.Columns["mainAmount"].ColumnName = "Үндсэн төлбөрийн эхний үлдэгдэл";
			if (dataTable.Columns["interestAmount"] != null)
				dataTable.Columns["interestAmount"].ColumnName = "Хүүгийн эхний үлдэгдэл";
			if (dataTable.Columns["leverageAmount"] != null)
				dataTable.Columns["leverageAmount"].ColumnName = "Хөшүүргийн эхний үлдэгдэл";
			if (dataTable.Columns["otherPay"] != null)
				dataTable.Columns["otherPay"].ColumnName = "Бусад төлбөр";
			if (dataTable.Columns["overpay"] != null)
				dataTable.Columns["overpay"].ColumnName = "Илүү төлөлт";
			if (dataTable.Columns["loanRealBalance"] != null)
				dataTable.Columns["loanRealBalance"].ColumnName = "Бодит үлдэгдэл";
			if (dataTable.Columns["paymentOverDay"] != null)
				dataTable.Columns["paymentOverDay"].ColumnName = "Хугацаа хэтэрсэн хоног";
			if (dataTable.Columns["register"] != null)
				dataTable.Columns["register"].ColumnName = "РД";
			if (dataTable.Columns["customerNameExcel"] != null)
				dataTable.Columns["customerNameExcel"].ColumnName = "Зээлдэгчийн нэр";
			//Хөшүүргийн жагсаалт дэлгэрэнгүй
			if (dataTable.Columns["leverDate"] != null)
				dataTable.Columns["leverDate"].ColumnName = "Хөшүүргийн огноо";
			if (dataTable.Columns["unPaidAmount"] != null)
				dataTable.Columns["unPaidAmount"].ColumnName = "Хөшүүрэг тооцох дүн";
			if (dataTable.Columns["leverAmount"] != null)
				dataTable.Columns["leverAmount"].ColumnName = "Хөшүүргийн дүн";
			if (dataTable.Columns["returnLeverAmount"] != null)
				dataTable.Columns["returnLeverAmount"].ColumnName = "Буцаасан хөшүүрэг";
			if (dataTable.Columns["returnDate"] != null)
				dataTable.Columns["returnDate"].ColumnName = "Буцаалтын огноо";
			if (dataTable.Columns["leverIncAmount"] != null)
				dataTable.Columns["leverIncAmount"].ColumnName = "Хөшүүргийн өссөн дүн";
			if (dataTable.Columns["leverApprovedAmount"] != null)
				dataTable.Columns["leverApprovedAmount"].ColumnName = "Батлагдсан дүн";
			if (dataTable.Columns["approvedDate"] != null)
				dataTable.Columns["approvedDate"].ColumnName = "Батлагдсан огноо";
			//Хөшүүргийн жагсаалт 
			if (dataTable.Columns["expStartDate"] != null)
				dataTable.Columns["expStartDate"].ColumnName = "Эхэлсэн огноо";
			if (dataTable.Columns["expEndDate"] != null)
				dataTable.Columns["expEndDate"].ColumnName = "Дууссан огноо";
			if (dataTable.Columns["exploanNumber"] != null)
				dataTable.Columns["exploanNumber"].ColumnName = "Зээлийн дугаар";
			if (dataTable.Columns["expCustomerName"] != null)
				dataTable.Columns["expCustomerName"].ColumnName = "Зээлдэгч";
			if (dataTable.Columns["expLeverageType"] != null)
				dataTable.Columns["expLeverageType"].ColumnName = "Хөшүүргийн төрөл";
			if (dataTable.Columns["expAllLeverageAmount"] != null)
				dataTable.Columns["expAllLeverageAmount"].ColumnName = "Тооцоологдсон дүн";
			if (dataTable.Columns["expAllReturnAmount"] != null)
				dataTable.Columns["expAllReturnAmount"].ColumnName = "Буцаасан дүн";
			if (dataTable.Columns["expAllBeforeApproveAmount"] != null)
				dataTable.Columns["expAllBeforeApproveAmount"].ColumnName = "Батлахаас өмнөх дүн";
			if (dataTable.Columns["expApprovedPercentage"] != null)
				dataTable.Columns["expApprovedPercentage"].ColumnName = "Батлагдсан хувь";
			if (dataTable.Columns["expApprovedAmount"] != null)
				dataTable.Columns["expApprovedAmount"].ColumnName = "Батлагдсан дүн";
			if (dataTable.Columns["expOverdueDays"] != null)
				dataTable.Columns["expOverdueDays"].ColumnName = "Хугацаа хэтэрсэн хоног";
			//Зээлийн мэдээ
			if (dataTable.Columns["rCustomerName"] != null)
				dataTable.Columns["rCustomerName"].ColumnName = "Нэрс";
			if (dataTable.Columns["rCustomerRegister"] != null)
				dataTable.Columns["rCustomerRegister"].ColumnName = "Регистер";
			if (dataTable.Columns["rCustomerType"] != null)
				dataTable.Columns["rCustomerType"].ColumnName = "Зээлдэгчийн төрөл";
			if (dataTable.Columns["rLoanClassification"] != null)
				dataTable.Columns["rLoanClassification"].ColumnName = "Дотоод ангилал";
			if (dataTable.Columns["rLoanState"] != null)
				dataTable.Columns["rLoanState"].ColumnName = "Зээлийн төлөв";
			if (dataTable.Columns["rLoanPurpose"] != null)
				dataTable.Columns["rLoanPurpose"].ColumnName = "Зориулалт";
			if (dataTable.Columns["rLoanDate"] != null)
				dataTable.Columns["rLoanDate"].ColumnName = "Олгосон огноо";
			if (dataTable.Columns["rLoanInterest"] != null)
				dataTable.Columns["rLoanInterest"].ColumnName = "Хүү /жил/";
			if (dataTable.Columns["rLoanTime"] != null)
				dataTable.Columns["rLoanTime"].ColumnName = "Хугацаа";
			if (dataTable.Columns["rLoanLeverage"] != null)
				dataTable.Columns["rLoanLeverage"].ColumnName = "Хөшүүргийн төрөл";
			if (dataTable.Columns["rLoanAmount"] != null)
				dataTable.Columns["rLoanAmount"].ColumnName = "Олгосон хэмжээ";
			if (dataTable.Columns["rRepaymentAll"] != null)
				dataTable.Columns["rRepaymentAll"].ColumnName = "Нийт эргэн төлөлт";
			if (dataTable.Columns["rRepaymentAmount"] != null)
				dataTable.Columns["rRepaymentAmount"].ColumnName = "Төлөгдсөн үндсэн зээл";
			if (dataTable.Columns["rRepaymentInterest"] != null)
				dataTable.Columns["rRepaymentInterest"].ColumnName = "Төлсөн хүү";
			if (dataTable.Columns["rRepaymentLeverage"] != null)
				dataTable.Columns["rRepaymentLeverage"].ColumnName = "Төлсөн хөшүүрэг";
			if (dataTable.Columns["rUnpayAll"] != null)
				dataTable.Columns["rUnpayAll"].ColumnName = "Нийт зөрчлийн дүн";
			if (dataTable.Columns["rUnpayAmount"] != null)
				dataTable.Columns["rUnpayAmount"].ColumnName = "Төлөгдөөгүй үндсэн зээл";
			if (dataTable.Columns["rUnpayInterest"] != null)
				dataTable.Columns["rUnpayInterest"].ColumnName = "Төлөөгүй хүү";
			if (dataTable.Columns["rUnpayLeverage"] != null)
				dataTable.Columns["rUnpayLeverage"].ColumnName = "Төлөөгүй хөшүүрэг";
			if (dataTable.Columns["rOverDueDays"] != null)
				dataTable.Columns["rOverDueDays"].ColumnName = "Нийт зөрчлийн хоног";
			if (dataTable.Columns["rUnpayAmountAll"] != null)
				dataTable.Columns["rUnpayAmountAll"].ColumnName = "Үндсэн зээлийн үлдэгдэл";
			if (dataTable.Columns["rResolvedAmount"] != null)
				dataTable.Columns["rResolvedAmount"].ColumnName = "Шүүхэд нэхэмжлэх дүн";
			if (dataTable.Columns["rMongolBankClass"] != null)
				dataTable.Columns["rMongolBankClass"].ColumnName = "Монгол банкны ангилал";
			if (dataTable.Columns["rProvide"] != null)
				dataTable.Columns["rProvide"].ColumnName = "Аймаг";
			if (dataTable.Columns["rPhoneNumber"] != null)
				dataTable.Columns["rPhoneNumber"].ColumnName = "Холбогдох утас";

			return dataTable;
		}
		public static byte[] ExportExcel(DataTable dataTable, string heading = "", bool showSrNo = false, params string[] columnsToTake)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				ExcelWorksheet workSheet = package.Workbook.Worksheets.Add(String.Format("{0}", heading));
				int startRowFrom = String.IsNullOrEmpty(heading) ? 1 : 3;
				if (showSrNo)
				{
					DataColumn dataColumn = dataTable.Columns.Add("№", typeof(int));
					dataColumn.SetOrdinal(0);
					int index = 1;
					foreach (DataRow item in dataTable.Rows)
					{
						item[0] = index;
						index++;
					}
				}
				// add the content into the Excel file
				workSheet.Cells["A" + startRowFrom].LoadFromDataTable(dataTable, true);
				// autofit width of cells with small content
				int columnIndex = 1;
				foreach (DataColumn column in dataTable.Columns)
				{
					try
					{
						if (column.DataType == typeof(DateTime))
						{
							workSheet.Column(columnIndex).Style.Numberformat.Format = "yyyy-MM-dd";
						}
						ExcelRange columnCells = workSheet.Cells[workSheet.Dimension.Start.Row, columnIndex, workSheet.Dimension.End.Row, columnIndex];
						int maxLength = columnCells.Max(x => x.Value.ToString().Count());
						workSheet.Column(columnIndex).AutoFit();
						if (maxLength > 50)
						{
							workSheet.Column(columnIndex).Style.WrapText = true;
						}
						columnIndex++;
					}
					catch (Exception ex)
					{
						if (!(ex is System.Threading.ThreadAbortException))
						{
							//Log other errors here
						}
					}

				}
				// format header - bold, yellow on black
				using (ExcelRange r = workSheet.Cells[startRowFrom, 1, startRowFrom, dataTable.Columns.Count])
				{
					r.Style.Font.Color.SetColor(System.Drawing.Color.Black);
					r.Style.Font.Bold = true;
					r.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
					r.Style.Border.Top.Style = ExcelBorderStyle.Thin;
					r.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
					r.Style.Border.Left.Style = ExcelBorderStyle.Thin;
					r.Style.Border.Right.Style = ExcelBorderStyle.Thin;
					r.Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
					r.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
					r.Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
					r.Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
					r.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#d3d6e0"));
				}
				// format cells - add borders
				using (ExcelRange r = workSheet.Cells[startRowFrom, 1, startRowFrom + dataTable.Rows.Count, dataTable.Columns.Count])
				{
					r.Style.Border.Top.Style = ExcelBorderStyle.Thin;
					r.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
					r.Style.Border.Left.Style = ExcelBorderStyle.Thin;
					r.Style.Border.Right.Style = ExcelBorderStyle.Thin;
					r.Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
					r.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
					r.Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
					r.Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
				}
				//removed ignored columns
				for (int i = dataTable.Columns.Count - 1; i >= 0; i--)
				{
					if (i == 0 && showSrNo)
					{
						continue;
					}
					if (!columnsToTake.Contains(dataTable.Columns[i].ColumnName))
					{
						workSheet.DeleteColumn(i + 1);
					}
				}
				if (!String.IsNullOrEmpty(heading))
				{
					workSheet.Cells["A1"].Value = heading;
					workSheet.Cells["A1"].Style.Font.Size = 20;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}
		public static byte[] ExportExcel<T>(List<T> data, string Heading = "", bool showSlno = false, params string[] ColumnsToTake)
		{
			return ExportExcel(ListToDataTable<T>(data), Heading, showSlno, ColumnsToTake);
		}

        public static byte[] ExportExcelUnalloc<T>(List<T> data, string Heading = "", bool showSlno = false, string repType = "", params string[] ColumnsToTake)
        {
            return ExportExcel(ListToDataTable<T>(data, repType), Heading, showSlno, ColumnsToTake);
        }
    }
}