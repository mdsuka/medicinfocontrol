﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedicInfoControl.Models {
  [Table("province")]
  public class Province {
    [Key]
    public int id { get; set; }
    [DisplayName("Аймаг/Нийслэлийн код")]
    [Required(ErrorMessage = "Аймаг/Нийслэлийн кодоо оруулна уу!")]
    [StringLength(50, ErrorMessage = "Талбарын урт хамгийн уртдаа 50 байна.")]
    [Remote("provinceExistsCode", "Location", AdditionalFields = "id", HttpMethod = "POST", ErrorMessage = "Код бүртгэгдсэн тул солино уу!")]
    public string code { get; set; }
    [DisplayName("Аймаг/Нийслэлийн нэр")]
    [Required(ErrorMessage = "Аймаг/Нийслэлийн нэрээ оруулна уу!")]
    [StringLength(50, ErrorMessage = "Талбарын урт хамгийн уртдаа 50 байна.")]
    [Remote("provinceExistsName", "Location", AdditionalFields = "id", HttpMethod = "POST", ErrorMessage = "Нэр бүртгэгдсэн тул солино уу!")]
    public string name { get; set; }
    public string countryId { get; set; }
    public string prefix { get; set; }
    public IEnumerable<SelectListItem> cntryList { get; set; }
  }
}