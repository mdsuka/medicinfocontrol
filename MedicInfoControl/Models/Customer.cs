﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace MedicInfoControl.Models {
  [Table("customer")]
  public class Customer {
    [Key]
    public int id { get; set; }
    [DisplayName("Идэвхигүй")]
    public int isActive { get; set; }
    [DisplayName("ДД")]
    [Required(ErrorMessage = "Харилцагчийн кодоо оруулна уу!")]
    [StringLength(20, MinimumLength = 4, ErrorMessage = "4-с их 20-оос бага оронтой тоо болон '/-' тэмдэгт зөвшөөрнө.")]
    [RegularExpression("^[0-9-/]+$", ErrorMessage = "Зөвхөн тоо оруулна уу!")]
    public string code { get; set; }
    [DisplayName("Регистр")]
    [Required(ErrorMessage = "Регистрээ оруулна уу!")]
    [StringLength(50, ErrorMessage = "Талбарын урт хамгийн уртдаа 50 байна.")]
    public string regNumber { get; set; }
    [DisplayName("Аймаг/Нийслэл")]
    [Required(ErrorMessage = "Аймаг/Нийслэл-ээ сонгоно уу!")]
    public int? province { get; set; }
    [DisplayName("Сум/Дүүрэг")]
    [Required(ErrorMessage = "Сум/Дүүрэг-ээ сонгоно уу!")]
    public int? district { get; set; }
    [DisplayName("Ургийн овог")]
    [Required(ErrorMessage = "Ургийн овгоо оруулна уу!")]
    public string familyName { get; set; }
    [DisplayName("Эцэг, эхийн нэр")]
    [Required(ErrorMessage = "Эцэг, эхийн нэрээ оруулна уу!")]
    public string lastName { get; set; }
    [DisplayName("Өөрийн нэр")]
    [Required(ErrorMessage = "Өөрийн нэрээ оруулна уу!")]
    public string firstName { get; set; }
    [DisplayName("Хороо,байр,хаалга")]
    [Required(ErrorMessage = "Хороо,байр,хаалгаа оруулна уу!")]
    public string address { get; set; }
    [DisplayName("Утас")]
    [Required(ErrorMessage = "Утсаа оруулна уу!")]
    public string mobilePhone { get; set; }
    [DisplayName("Цахим шуудан")]
    public string email { get; set; }
    [DisplayName("Биеийн жин")]
    public double customerWeight { get; set; }
    [NotMapped]
    public string mergCusName { get; set; }
    [NotMapped]
    public string provinceMap { get; set; }
    [NotMapped]
    public string budLevelName { get; set; }
    [NotMapped]
    public string districtMap { get; set; }
    [NotMapped]
    public string cusTypeName { get; set; }
    public IEnumerable<SelectListItem> internalListBox { get; set; }
    public IEnumerable<SelectListItem> provincListBox { get; set; }
    public IEnumerable<SelectListItem> districtListBox { get; set; }
    //[NotMapped]
    //public List<CustomerOther> custmrOther { get; set; }
  }
}