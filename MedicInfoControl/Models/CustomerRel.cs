﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MedicInfoControl.Models {
  public class CustomerRel {
    public int medicineId { get; set; }
    public string customerId { get; set; }
  }
}