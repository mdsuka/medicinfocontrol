﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Diagnostics;
using System.Threading;
using System.Web;
using System.Web.Configuration;
namespace MedicInfoControl.Models {
  public partial class FModelContext : DbContext {
    public FModelContext() : base("name = FModelContext") {
      //try {
      //  if (HttpContext.Current != null) {
      //    string dbName = "";
      //    dbName = HttpContext.Current.Session["DbName"] as string;
      //    string databaseName = Database.Connection.Database;
      //    if (!dbName.Equals(databaseName)) {
      //      if (HttpContext.Current.Session != null && HttpContext.Current.Session["DbName"] != null) {
      //        dbName = Convert.ToString(HttpContext.Current.Session["DbName"]);
      //        //Configuration Config1 = WebConfigurationManager.OpenWebConfiguration("~");
      //        //ConnectionStringsSection conSetting = (ConnectionStringsSection)Config1.GetSection("connectionStrings");
      //        string constring = ConfigurationManager.ConnectionStrings["FModelContext"].ConnectionString;
      //        string[] conList = constring.Split(';');
      //        if (conList.Length > 3) {
      //          conList[3] = "database=" + dbName;
      //        }

      //        //int index = constring.IndexOf("database");
      //        //string firstStr = constring.Substring(0, index);

      //        //string lastStr = "database=" + dbName + ";Port=3306;charset = utf8; SSlmode = none; AllowUserVariables=True";
      //        this.Database.Connection.ConnectionString = String.Join(";", conList);
      //      }
      //    }
      //  }
      //} catch (Exception ex) {
      //  Trace.WriteLine(ex);
      //}
      //System.Configuration.Configuration Config1 = WebConfigurationManager.OpenWebConfiguration("~");
      //ConnectionStringsSection conSetting = (ConnectionStringsSection)Config1.GetSection("connectionStrings");
      //string constring = ConfigurationManager.ConnectionStrings["FModelContext"].ConnectionString;
      //int index = constring.IndexOf("database");
      //string dbbName = constring.Substring(index);
    }
    public virtual DbSet<Customer> customerMdl { get; set; }
    public virtual DbSet<TreatmentSchedule> treatmentScheduleMdl { get; set; }
    public virtual DbSet<MedicineSchedule> medicineScheduleMdl { get; set; }
    public virtual DbSet<MedicineUse> medicineUseMdl { get; set; }
    public virtual DbSet<Province> provinceMdl { get; set; }
    public virtual DbSet<District> districtMdl { get; set; }
    public virtual DbSet<CusRelevantInfo> cusRelevantInfo { get; set; }
    public virtual DbSet<TreatmentResult> treatmentResult { get; set; }
  }
}