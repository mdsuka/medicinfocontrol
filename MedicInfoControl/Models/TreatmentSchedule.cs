﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MedicInfoControl.Models {
  public class TreatmentSchedule {
    [Key]
    public int id { get; set; }
    public int medicineId { get; set; }
    public int customerId { get; set; }
    [DisplayName("Эмийн нэр")]
    public string medicineName { get; set; }
    [DisplayName("Уух огноо")]
    public DateTime? scheduledDate { get; set; }
    [DisplayName("Уусан огноо")]
    public DateTime? drunkTime { get; set; }

    [NotMapped]
    [DisplayName("Нэр")]
    public string customerName { get; set; }
    [NotMapped]
    [DisplayName("Уух огноо")]
    public string scheduleDate { get;set; }
    [NotMapped]
    [DisplayName("Уусан огноо")]
    public string drinkTime { get;  set; }
  }
}