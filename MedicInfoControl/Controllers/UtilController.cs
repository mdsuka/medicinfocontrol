﻿using MedicInfoControl.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
namespace MedicInfoControl.Controllers {
  public class UtilController : BaseController {
    private Random random = new Random();
    private List<string> accList8 = new List<string>(new string[] { "120002", "120004" });
    private Hashtable reportHs = new Hashtable(){
      { "CTT1", "\'002\',\'1\',\'028\'" },
      { "CTT2", "\'003\'" },
      { "CTT3", "\'004\'" },
      { "CTT4", "\'005\'" },
      { "CTT6", "\'007\'" },
      { "CTT8", "\'010\',\'011\'" },
      { "CTT9", "\'012\'" }
    };
    //Нууц үг encrypt хийх
    public string Encrypt(string clearText) {
      string EncryptionKey = "MAKV2SPBNI99212";
      if (clearText != "" && clearText != null) {
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create()) {
          Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
          encryptor.Key = pdb.GetBytes(32);
          encryptor.IV = pdb.GetBytes(16);
          using (MemoryStream ms = new MemoryStream()) {
            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)) {
              cs.Write(clearBytes, 0, clearBytes.Length);
              cs.Close();
            }
            clearText = Convert.ToBase64String(ms.ToArray());
          }
        }
      }
      return clearText;
    }
    #region dropdown
    public IEnumerable<SelectListItem> GetAllProvince() {
      var stateList = Database.provinceMdl
                .Select(x => new SelectListItem {
                  Value = x.id.ToString(),
                  Text = x.code + ")  " + x.name
                });
      List<SelectListItem> _list = stateList.ToList();
      
      return new SelectList(_list, "Value", "Text");
    }

    public IEnumerable<SelectListItem> GetAllDistrict() {
      var stateList = Database.districtMdl
                .Select(x => new SelectListItem {
                  Value = x.id.ToString(),
                  Text = x.code + ")  " + x.name
                });
      List<SelectListItem> _list = stateList.ToList();
      _list.Insert(0, new SelectListItem() { Value = "", Text = null });
      return new SelectList(_list, "Value", "Text");
    }

    public IEnumerable<SelectListItem> GetAllCustomer() {
      var stateList = Database.customerMdl
                .Select(x => new SelectListItem {
                  Value = x.id.ToString(),
                  Text = x.firstName + " " + x.lastName
                });
      List<SelectListItem> _list = stateList.ToList();
      _list.Insert(0, new SelectListItem() { Value = "", Text = null });
      return new SelectList(_list, "Value", "Text");
    }

    public IEnumerable<SelectListItem> GetAllMedicine() {
      var stateList = Database.medicineUseMdl
                .Select(x => new SelectListItem {
                  Value = x.medicineId.ToString(),
                  Text = x.medicineNameMng
                });
      List<SelectListItem> _list = stateList.ToList();
      _list.Insert(0, new SelectListItem() { Value = "", Text = null });
      return new SelectList(_list, "Value", "Text");
    }


    #endregion

    public IEnumerable<SelectListItem> GetAllPath() {
      Assembly asm = Assembly.GetAssembly(typeof(MvcApplication));
      List<SelectListItem> rsList = new List<SelectListItem>();
      List<string> lst = new List<string>();
      var controlleractionlist = asm.GetTypes()
              .Where(type => typeof(System.Web.Mvc.Controller).IsAssignableFrom(type))
              .SelectMany(type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
              .Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any())
              .Select(x => new { Controller = x.DeclaringType.Name, Action = x.Name, ReturnType = x.ReturnType.Name, Attributes = String.Join(",", x.GetCustomAttributes().Select(a => a.GetType().Name.Replace("Attribute", ""))) })
              .OrderBy(x => x.Controller).ThenBy(x => x.Action).ToList();

      foreach (var ssss in controlleractionlist) {
        if (!lst.Contains(ssss.Controller + "/" + ssss.Action) && ssss.ReturnType == "ActionResult") {
          lst.Add(ssss.Controller + "/" + ssss.Action);
          rsList.Add(new SelectListItem {
            Value = ssss.Controller.Replace("Controller", "") + "/" + ssss.Action,
            Text = ssss.Controller.Replace("Controller", "") + "/" + ssss.Action
          });
        }
      }
      rsList.Insert(0, new SelectListItem() { Value = "", Text = null });
      return new SelectList(rsList, "Value", "Text");
    }
  }
}