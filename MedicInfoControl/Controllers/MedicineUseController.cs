﻿using MedicInfoControl.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedicInfoControl.Controllers {
  public class MedicineUseController : BaseController {
    // GET: MedicineUse
    public ActionResult Index() {
      List<MedicineUse> cus = new List<MedicineUse>();
      return PartialView(cus);
    }

    [HttpPost]
    public ActionResult GetList() {
      JsonResult result = new JsonResult();
      try {
        List<MedicineUse> clist = Database.medicineUseMdl.ToList();
        List<Customer> cust = Database.customerMdl.ToList();

        var data = clist;
        string search = Request.Form.GetValues("search[value]")[0];
        string draw = Request.Form.GetValues("draw")[0];
        string order = Request.Form.GetValues("order[0][column]")[0];
        string orderDir = Request.Form.GetValues("order[0][dir]")[0];
        int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
        int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
        var state = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
        int totalRecords = data.Count;
        //if (!string.IsNullOrEmpty(search) &&
        //    !string.IsNullOrWhiteSpace(search) && search != "") {
        //  data = data.Where(
        //    p => (p.code != null && p.code.ToLower().Contains(search.ToLower()) ||
        //     p.code != null && p.code.ToLower().Contains(search.ToLower()) ||
        //     p.name != null && p.name.ToLower().Contains(search.ToLower()) ||
        //     p.regNumber != null && p.regNumber.ToLower().Contains(search.ToLower()) ||
        //     p.provinceMap != null && p.provinceMap.ToLower().Contains(search.ToLower()) ||
        //     p.districtMap != null && p.districtMap.ToLower().Contains(search.ToLower()) ||
        //     p.cusTypeName != null && p.cusTypeName.ToLower().Contains(search.ToLower()) ||
        //     p.familyName != null && p.familyName.ToLower().Contains(search.ToLower()) ||
        //     p.lastName != null && p.lastName.ToLower().Contains(search.ToLower()) ||
        //     p.firstName != null && p.firstName.ToLower().Contains(search.ToLower()) ||
        //     p.address != null && p.address.ToLower().Contains(search.ToLower()) ||
        //     p.mobilePhone != null && p.mobilePhone.ToLower().Contains(search.ToLower()) ||
        //     p.email != null && p.email.ToLower().Contains(search.ToLower())
        //     )).ToList();
        //}
        //data = this.SortByColumnWithOrder(order, orderDir, data);
        int recFilter = data.Count;
        data = data.Skip(startRec).Take(pageSize).ToList();
        result = this.Json(new {
          draw = Convert.ToInt32(draw),
          recordsTotal = totalRecords,
          recordsFiltered = recFilter,
          data = data
        }, JsonRequestBehavior.AllowGet);
      } catch (Exception ex) {
        Console.WriteLine(ex);
      }
      return result;
    }

    public ActionResult CreateMedicineUse(string id) {
      MedicineUse cus = new MedicineUse();
      if (!string.IsNullOrEmpty(id)) {
        MedicineUse dbCustom = Database.medicineUseMdl.Find(Convert.ToInt32(id));
        cus.medicineId = dbCustom.medicineId;
        cus.medicineNameMng = dbCustom.medicineNameMng;
        cus.medicineNameEng = dbCustom.medicineNameEng;
        cus.medicineUnit = dbCustom.medicineUnit;
        cus.treatmentName = dbCustom.treatmentName;
        cus.frequencyMed = dbCustom.frequencyMed;
      }
      return PartialView(cus);
    }
    [HttpPost]
    public ActionResult CreateMedicineUse(MedicineUse dbCustom) {
      MedicineUse cus = Database.medicineUseMdl.Find(dbCustom.medicineId);
      using (var transaction = Database.Database.BeginTransaction()) {
        if (cus != null) {
          cus.medicineNameMng = dbCustom.medicineNameMng;
          cus.medicineNameEng = dbCustom.medicineNameEng;
          cus.medicineUnit = dbCustom.medicineUnit;
          cus.treatmentName = dbCustom.treatmentName;
          cus.frequencyMed = dbCustom.frequencyMed;
        } else {
          Database.medicineUseMdl.Add(dbCustom);
        }
        try {
          Database.SaveChanges();
          transaction.Commit();
        } catch (DbEntityValidationException ex) {
          transaction.Rollback();
          foreach (var entityValidationErrors in ex.EntityValidationErrors) {
            foreach (var validationError in entityValidationErrors.ValidationErrors) {
              Debug.WriteLine("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
            }
          }
          return Redirect("Index");
        } catch (Exception ee) {
          throw ee;
        }
      }
      return Redirect("Index");
    }
    public ActionResult DeleteMedicine(int id) {
      MedicineUse cust = Database.medicineUseMdl.SingleOrDefault(x => x.medicineId == id);
      Database.medicineUseMdl.Remove(cust);
      Database.Entry(cust).State = System.Data.Entity.EntityState.Deleted;
      Database.ChangeTracker.DetectChanges();
      Database.SaveChanges();
      return RedirectToAction("Index");
    }
  }
}