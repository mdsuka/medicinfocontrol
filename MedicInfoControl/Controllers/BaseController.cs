﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedicInfoControl.Models;

namespace MedicInfoControl.Controllers {
  public class BaseController : Controller {
    public BaseController() { }

    private FModelContext _database;
    protected FModelContext Database {
      get {
        if (_database == null)
          _database = new FModelContext();
        return _database;
      }
    }

    protected override void Dispose(bool disposing) {
      if (_database != null)
        _database.Dispose();
      base.Dispose(disposing);
    }
  }
}