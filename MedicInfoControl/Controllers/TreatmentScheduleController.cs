﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedicInfoControl.Models;

namespace MedicInfoControl.Controllers {
  public class TreatmentScheduleController : BaseController {
    UtilController ut = new UtilController();
    // GET: TreatmentSchedule
    public ActionResult Index() {
      List<MedicineSchedule> cus = new List<MedicineSchedule>();
      return PartialView(cus);
    }

    [HttpPost]
    public ActionResult GetList() {
      JsonResult result = new JsonResult();
      try {
        List<CommonMdl> cmlist = GetUserList();
        List<MedicineSchedule> clist = new List<MedicineSchedule>();
        foreach(CommonMdl cmdl in cmlist) {
          MedicineSchedule mm = new MedicineSchedule();
          mm.vodUserId = cmdl.userid;
          mm.startDts = cmdl.startdts;
          mm.endDts = cmdl.endts;
          clist.Add(mm);
        }
        List<Customer> cust = Database.customerMdl.ToList();
        List<MedicineUse> muse = Database.medicineUseMdl.ToList();

        foreach (MedicineSchedule ts in clist) {
          Customer cs = cust.FirstOrDefault(x => x.id == ts.vodUserId);
          ts.customerName = cs.firstName.Substring(0, 1) + ". " + cs.lastName;
        }
        var data = clist;
        string search = Request.Form.GetValues("search[value]")[0];
        string draw = Request.Form.GetValues("draw")[0];
        string order = Request.Form.GetValues("order[0][column]")[0];
        string orderDir = Request.Form.GetValues("order[0][dir]")[0];
        int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
        int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
        var state = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
        int totalRecords = data.Count;
        int recFilter = data.Count;
        data = data.Skip(startRec).Take(pageSize).ToList();
        result = this.Json(new {
          draw = Convert.ToInt32(draw),
          recordsTotal = totalRecords,
          recordsFiltered = recFilter,
          data = data
        }, JsonRequestBehavior.AllowGet);
      } catch (Exception ex) {
        Console.WriteLine(ex);
      }
      return result;
    }

    // GET: TreatmentSchedule/Edit/5
    public ActionResult CreateAndEdit(String id) {
      MedicineSchedule trsc = new MedicineSchedule();
      if (!string.IsNullOrEmpty(id)) {
        trsc = Database.medicineScheduleMdl.Where(x => x.id == Convert.ToInt32(id)).FirstOrDefault();
      }
      trsc.cusListBox = ut.GetAllCustomer();
      trsc.medListBox = ut.GetAllMedicine();
      return View(trsc);
    }

    // POST: TreatmentSchedule/Edit/5
    [HttpPost]
    public ActionResult CreateAndEdit(MedicineSchedule trsc) {
      MedicineSchedule trscEx = new MedicineSchedule();
      using (var transaction = Database.Database.BeginTransaction()) {
        try {
          if (trsc.id > 0) {
            trscEx.medicineId = trsc.medicineId;
            trscEx.vodUserId = trsc.vodUserId;
            trscEx.startDt = trsc.startDt;
            trscEx.endDt = trsc.endDt;
            List<MedicineSchedule> lsmed = DivideTimeRangeIntoIntervals(trsc);
          } else {
            List<MedicineSchedule> lsmed = DivideTimeRangeIntoIntervals(trsc);
            foreach (MedicineSchedule meds in lsmed) {
              Database.medicineScheduleMdl.Add(meds);
            }
          }
          try {
            Database.SaveChanges();
            transaction.Commit();
          } catch (DbEntityValidationException ex) {
            transaction.Rollback();
            foreach (var entityValidationErrors in ex.EntityValidationErrors) {
              foreach (var validationError in entityValidationErrors.ValidationErrors) {
                Debug.WriteLine("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
              }
            }
            return Redirect("Index");
          } catch (Exception ee) {
            throw ee;
          }
          return RedirectToAction("Index");
        } catch (Exception ex) {
          throw ex;
        }
      }
    }

    public ActionResult DeleteSchedule(int id) {
      MedicineSchedule cust = Database.medicineScheduleMdl.SingleOrDefault(x => x.id == id);
      Database.medicineScheduleMdl.Remove(cust);
      Database.Entry(cust).State = System.Data.Entity.EntityState.Deleted;
      Database.ChangeTracker.DetectChanges();
      Database.SaveChanges();
      return RedirectToAction("Index");
    }

    private List<CommonMdl> GetUserList() {
      try {
        List<CommonMdl> lst = Database.Database.SqlQuery<CommonMdl>("SELECT voduserid as userid, DATE_FORMAT(min(startDt),'%Y-%m-%d') as startdts, DATE_FORMAT(max(endDt),'%Y-%m-%d') as endts FROM medicineschedule group by voduserid").ToList();
        return lst;
      } catch (Exception ex) {
        throw ex;
      }
    }

    List<MedicineSchedule> DivideTimeRangeIntoIntervals(MedicineSchedule meds) {
      List<MedicineSchedule> retMed = new List<MedicineSchedule>();
      int numberOfIntervals = Database.medicineUseMdl.FirstOrDefault(x => x.medicineId == meds.medicineId).frequencyMed;
      long startTSInTicks = meds.startDt.Ticks;
      long endTsInTicks = meds.endDt.Ticks;
      long tickSpan = meds.endDt.Ticks - meds.startDt.Ticks;
      if (numberOfIntervals == 0)
        numberOfIntervals = 30;
      long tickInterval = tickSpan / numberOfIntervals;

      List<DateTime> listOfDates = new List<DateTime>();
      for (long i = startTSInTicks; i <= endTsInTicks; i += tickInterval) {
        MedicineSchedule med = new MedicineSchedule();
        med.medicineId = meds.medicineId;
        med.vodUserId = meds.vodUserId;
        med.startDt = new DateTime(i);
        med.endDt = meds.endDt;
        retMed.Add(med);
      }
      return retMed;
    }
  }
}
