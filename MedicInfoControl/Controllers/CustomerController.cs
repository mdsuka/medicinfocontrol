﻿using MedicInfoControl.Models;
using MedicInfoControl.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Org.BouncyCastle.Bcpg;

namespace MedicInfoControl.Controllers {
  public class CustomerController : BaseController {
    UtilController ut = new UtilController();
    public ActionResult IndexCustomer() {
      List<Customer> cus = new List<Customer>();
      return PartialView(cus);
    }
    [HttpPost]
    public ActionResult GetList() {
      JsonResult result = new JsonResult();
      try {
        List<Customer> clist = Database.customerMdl.ToList();
        var data = clist;
        string search = Request.Form.GetValues("search[value]")[0];
        string draw = Request.Form.GetValues("draw")[0];
        string order = Request.Form.GetValues("order[0][column]")[0];
        string orderDir = Request.Form.GetValues("order[0][dir]")[0];
        int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
        int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
        var state = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
        int totalRecords = data.Count;
        if (!string.IsNullOrEmpty(search) &&
            !string.IsNullOrWhiteSpace(search) && search != "") {
          data = data.Where(
            p => (p.code != null && p.code.ToLower().Contains(search.ToLower()) ||
             p.code != null && p.code.ToLower().Contains(search.ToLower()) ||
             p.regNumber != null && p.regNumber.ToLower().Contains(search.ToLower()) ||
             p.provinceMap != null && p.provinceMap.ToLower().Contains(search.ToLower()) ||
             p.districtMap != null && p.districtMap.ToLower().Contains(search.ToLower()) ||
             p.cusTypeName != null && p.cusTypeName.ToLower().Contains(search.ToLower()) ||
             p.familyName != null && p.familyName.ToLower().Contains(search.ToLower()) ||
             p.lastName != null && p.lastName.ToLower().Contains(search.ToLower()) ||
             p.firstName != null && p.firstName.ToLower().Contains(search.ToLower()) ||
             p.address != null && p.address.ToLower().Contains(search.ToLower()) ||
             p.mobilePhone != null && p.mobilePhone.ToLower().Contains(search.ToLower()) ||
             p.email != null && p.email.ToLower().Contains(search.ToLower())
             )).ToList();
        }
        data = this.SortByColumnWithOrder(order, orderDir, data);
        int recFilter = data.Count;
        data = data.Skip(startRec).Take(pageSize).ToList();
        result = this.Json(new {
          draw = Convert.ToInt32(draw),
          recordsTotal = totalRecords,
          recordsFiltered = recFilter,
          data = data
        }, JsonRequestBehavior.AllowGet);
      } catch (Exception ex) {
        throw ex;
      }
      return result;
    }

    private List<Customer> SortByColumnWithOrder(string order, string orderDir, List<Customer> data) {
      List<Customer> lst = data;
      try {
        switch (order) {
          case "1":
            lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.code).ToList() : data.OrderBy(p => p.code).ToList();
            break;
          case "4":
            lst = orderDir.Equals("ASC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.regNumber).ToList() : data.OrderBy(p => p.regNumber).ToList();
            break;
          case "5":
            lst = orderDir.Equals("ASC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.cusTypeName).ToList() : data.OrderBy(p => p.cusTypeName).ToList();
            break;
          case "7":
            lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.address).ToList() : data.OrderBy(p => p.address).ToList();
            break;
          case "8":
            lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.email).ToList() : data.OrderBy(p => p.email).ToList();
            break;
          case "9":
            lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.mobilePhone).ToList() : data.OrderBy(p => p.mobilePhone).ToList();
            break;
        }
      } catch (Exception ex) {
        Console.Write(ex);
      }
      return lst;
    }

    public ActionResult CreateCustomer(string id) {
      Customer cus = new Customer();
      if (!string.IsNullOrEmpty(id)) {
        Customer dbCustom = Database.customerMdl.Find(Convert.ToInt32(id));
        cus.id = dbCustom.id;
        cus.code = dbCustom.code;
        cus.regNumber = dbCustom.regNumber;
        cus.province = dbCustom.province;
        cus.district = dbCustom.district;
        cus.familyName = dbCustom.familyName;
        cus.lastName = dbCustom.lastName;
        cus.firstName = dbCustom.firstName;
        cus.address = dbCustom.address;
        cus.mobilePhone = dbCustom.mobilePhone;
        cus.email = dbCustom.email;
        cus.customerWeight = dbCustom.customerWeight;
      } else {
        string query = "select distinct cs.code from customer cs order by cast(cs.code as unsigned) desc limit 1";
        string codelist = Database.Database.SqlQuery<string>(query).FirstOrDefault();
        if (codelist == null) {
          cus.code = "1";
        } else {
          decimal a = Convert.ToDecimal(codelist) + 1;
          cus.code = a.ToString();
        }
      }

      cus.provincListBox = ut.GetAllProvince();
      cus.districtListBox = ut.GetAllDistrict();

      return PartialView(cus);
    }
    [HttpPost]
    public ActionResult CreateCustomer(Customer cus) {
      Customer dbCustom = Database.customerMdl.Find(cus.id);
      using (var transaction = Database.Database.BeginTransaction()) {
        if (dbCustom != null) {
          dbCustom.code = cus.code;
          dbCustom.regNumber = cus.regNumber;
          dbCustom.province = cus.province;
          dbCustom.district = cus.district;
          dbCustom.familyName = cus.familyName;
          dbCustom.lastName = cus.lastName;
          dbCustom.firstName = cus.firstName;
          dbCustom.address = cus.address;
          dbCustom.mobilePhone = cus.mobilePhone;
          dbCustom.email = cus.email;
          dbCustom.customerWeight = cus.customerWeight;
        } else {
          Database.customerMdl.Add(cus);
        }
        try {
          Database.SaveChanges();
          transaction.Commit();
          return RedirectToAction("IndexCustomer");
        } catch (DbEntityValidationException ex) {
          transaction.Rollback();
          foreach (var entityValidationErrors in ex.EntityValidationErrors) {
            foreach (var validationError in entityValidationErrors.ValidationErrors) {
              Debug.WriteLine("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
            }
          }
          return RedirectToAction("IndexCustomer");
        }
      }
      return RedirectToAction("IndexCustomer");
    }

    public ActionResult DeleteCustomer(int id) {
      Customer cust = Database.customerMdl.SingleOrDefault(x => x.id == id);
      Database.customerMdl.Remove(cust);
      Database.Entry(cust).State = System.Data.Entity.EntityState.Deleted;
      Database.ChangeTracker.DetectChanges();
      Database.SaveChanges();
      return RedirectToAction("IndexCustomer");
    }

    [HttpPost]
    public ActionResult CreateCustomerRelInfo() {
      return PartialView();
    }

    public ActionResult CreateCustomerRelInfo(Customer scs) {
      return PartialView();
    }
    [HttpGet]
    public FileContentResult CustomerToExcel() {
      string[] columns = { "Код", "Харилцагч", "Харилцагчийн төрөл", "Регистр", "Улс", "Аймаг/Нийслэл", "Сум дүүрэг", "Товч нэр", "Латин нэр", "Байгууллагын төрөл", "УБГД", "ТТБД", "Төсвийн захирагч ", "Дотоод ангилал", "Хаяг", "Үйл ажиллагааны чиглэл", "Утасны дугаар", "И-мэйл", "Гэрээний хугацаа", " Дансны нэр" };
      string sqlCus = "select cs.code, ms.name custp," +
                      "case when cs.state = '02' then cs.NAME " +
                      " when cs.state = '01' then concat(cs.firstName,' ',cs.lastName) " +
                      "else cs.name end as cusname, " +
                      "cs.regNumber, co.name cntr, pr.name prv, ds.name dstr, cs.shortName prefx, cs.englishName enname, " +
                      "(select name from mainstates ms where ms.code = cs.state) as state," +
                      "cs.certificateRegID certid, " +
                      "cs.taxpayer txp, bg.name as bgname, iw.name incat," +
                      "cs.address, cs.actionDirection act, cs.mobilePhone phn, cs.email eml, cs.contractDate ctdt " +
                      "from customer cs " +
                      "left outer join mainstates ms on cs.cusType = ms.id " +
                      "left outer join country co on cs.country = co.id " +
                      "left outer join province pr on cs.province = pr.id " +
                      "left outer join district ds on cs.district = ds.id " +
                      "left outer join budgetgovernor bg on cs.budGovernor = bg.id " +
                      "left outer join inwardlabel iw on cs.internalCategories = iw.id " +
                      "order by cs.code";
      List<CustomerListVM> cusListExportlist = Database.Database.SqlQuery<CustomerListVM>(sqlCus).ToList();
      byte[] filecontent = ExcelExport.ExportExcel(cusListExportlist, "Харилцагч", true, columns);
      return File(filecontent, ExcelExport.ExcelContentType, "CustomerList.xlsx");
    }

    public JsonResult DistrictProvinceId(string provinceCode) {
      int Id;
      if (provinceCode != "")
        Id = Convert.ToInt32(provinceCode);
      else
        Id = 0;
      string qry3 = "select d.id, d.code as number, d.name as number1 from district d where d.provinceid = '" + provinceCode + "' order by d.code ";
      List<CommonTotalVM> foot = Database.Database.SqlQuery<CommonTotalVM>(qry3).ToList();
      return Json(foot, JsonRequestBehavior.AllowGet);
    }

    public ActionResult CreateCustomerInfo(string id) {
      CusRelevantInfo cusInfo = new CusRelevantInfo();
      if (id != "") {
        cusInfo = Database.cusRelevantInfo.FirstOrDefault(x => x.customerId == id);
      }
      return PartialView(cusInfo);
    }
  }
}
