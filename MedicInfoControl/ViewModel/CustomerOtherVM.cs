﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MedicInfoControl.ViewModel {
  public class CustomerOtherVM {
    public string code { get; set; }
    public string name { get; set; }
    public string state { get; set; }
    public string regNumber { get; set; }
    public string lastName { get; set; }
    public string firstName { get; set; }
    public string telephone { get; set; }
    public string mobilePhone { get; set; }
    public string email { get; set; }
    public string address { get; set; }
    public string country { get; set; }
    public string province { get; set; }
    public string district { get; set; }
    public string orgaPhone { get; set; }
  }
  public class CustomerListVM {
    public string code { get; set; }
    public string custp { get; set; }
    public string cusname { get; set; }
    public string regNumber { get; set; }
    public string cntr { get; set; }
    public string prv { get; set; }
    public string dstr { get; set; }
    public string prefx { get; set; }
    public string enname { get; set; }
    public string certid { get; set; }
    public string txp { get; set; }
    public string bgname { get; set; }
    public string incat { get; set; }
    public string address { get; set; }
    public string act { get; set; }
    public string phn { get; set; }
    public string eml { get; set; }
    public string ctdt { get; set; }
    public string bankInfo { get; set; }
  }
  public class IdNameVM {
    public string code { get; set; }
    public string name { get; set; }
  }

    public class CustomerComputationAct
    {
        public string name { get; set; }
        public string regNumber { get; set; }
        public string address { get; set; }
        public string mobilePhone { get; set; }
        public string email { get; set; }        
    }
    public class CustomerBankComputationAct
    {
        public string bankName { get; set; }
        public string number { get; set; }        
    }
}